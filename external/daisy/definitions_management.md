<a name="DEF_M"></a>
# 6 Definitions Management 

With the _Definitions_ module, DAISY allows the management of _Contacts, _Cohorts_ and _Partners_. All pages within _Definitions_ operate according to the interface conventions given in the [relevant section of this guide](#DIC). In the following sections we only discuss what information is held in these definitions. 


<a name="COH_M"></a> 
## 6.1 Cohorts Management 
Cohort is a study that collects data and/or bio-samples from a group of participants (e.g. longitudinal case-control or family studies). A cohort is linked to the creation of data and is considered its ultimate source.

In order to effectively handle data subjects' requests, as per GDPR, it is crucial that an institution keeps track of what data it keeps from which cohorts. Inline with this, DAISY allows maintaining a list of _Cohorts_ and link  _Datasets_ to _Cohorts_. 

The information kept on cohorts can be seen in the associated _Editor Page_ seen below. Cohorts are expected to have a _Title_ unique to them, and they are  linked to one or more _Cohort Owners_, which are that are Principle Investigators, Clinicians running the study. Cohorts owners are kept as _Contacts_ in DAISY. In order to maintain a controlled list of cohorts, the administrator for the DAISY deployment may assign an accession number to the _Cohort_, which would be the unique identifier for this Cohort.  <br />
![Alt](./images/cohort_edit_form.png)


 <a name="PAR_M"></a> 
## 6.2 Partners Management

A _Partner_ is a research collaborator that is the source and/or recipient of human data. Partners are also legal entities with whom contracts are signed. Clinical entities that run longitudinal cohorts, research institutes, or data hubs are all examples of Partners. 
In accordance, when maintaining _Dataset_ source info, _Dataset_ transfer info or when creating _Contract_ records, you will be asked to select Partners.

The information kept on partners can be seen in the associated _Editor Page_ seen below. <br />
![Alt](./images/partner_edit_form.png)

<a name="CONN_M"></a> 
## 6.3 Contacts Management 

DAISY allows keeping an address book of all contact persons related to the _Projects_, _Datasets_, _Cohorts_ and _Contracts_. 

The information kept on contacts is pretty standard as can be seen in the associated _Editor Page_ given below. <br />
![Alt](./images/contact_edit_form.png)



<br />
<br />
<br />
<div style="text-align: right;"> <a href="#top">Back to top</a> </div>
<br />
<br />
<br />