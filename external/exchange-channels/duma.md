# Data Upload Manager - DUMA  Quick Guide

{{TOC}}

## Overview
DUMA is an IBM Aspera  deployment at the LCSB-University of Luxembourg. DUMA supports end-to-end encrypted data transfer and can handle high data volumes e.g. several tera bytes.

## Obtaining a DUMA access link

You need an **access link** to use DUMA. An access link is a temporary, password-protected space, much like a drop box, on LCSB's DUMA server. In order to obtain an access link, you should contact your research collaborator at the LCSB-University of Luxembourg. Once created, you will receive your **access link** and associated **password** (link) by e-mail.

**IMPORTANT NOTE: Whenever the LCSB shares a password for a DUMA endpoint (collaborator), the password be reachable via a link, which will expire. Therefore you should make a record of your password once you view it.**

An access link can be reached via standard web browsers. Data can be transferred to/from an access link in two ways:

* Through the web browser by visiting the link, which is our recommended way of data transfer, described in this [section of the guide](#DUMA_WEB)
* Through the use of a command line tool. If your data sits in an environment, where you can not launch a web browser, then you may use use a command line client tool to reach an access link. This process is described in this [section of this guide](#DUMA_CLI).  

The use of DUMA is mediated by LCSB's data stewards. If you require assistance in using DUMA, you should send an email to [LCSB admin team](lcsb-sysadmins@uni.lu).

<a name="DUMA_WEB"></a>
## Accessing DUMA via Web Interface

In the following steps we provide instructions on how to use DUMA web interface.

1. Once you receive your **access link** and **password** from the LCSB, visit the link using a standard web browser. Firefox 66.x (or higher) recommended, but others should also work. You will be prompted for your password (see below).<br/>
 ![Alt](duma_1.png "Title")

2. When you visit a DUMA access link for the very first time, you will be prompted to install **IBM Aspera Connect** client.
      * click **Download** or **Download latest version** links (see below). <br/>![Alt](duma_2.png "Title")



      * Your browser will download the client installer.

        ![Alt](duma_3.png "Title")


      * Run the installer till you receive the completion message.

        ![Alt](duma_4.png "Title")


      * Once **IBM Aspera Connect** client is installed, refresh the **access link**. You will be prompted whether you want to open **IBM Aspera Launcher**, click **Open**.

         ![Alt](duma_5.png "Title")


3. The **access link** page will display a **File Browser** section. Depending on the settings per access link, users can create or delete folders in the File Browser and upload or download data.<br/>
  ![Alt](duma_6.png "Title")



4. Clicking **Upload** or **Download** will launch the **IBM Aspera Connect** client on your computer. You first will be asked whether you allow the client to connect to aspera.lcsb.uni.lu. Choose **Allow**. <br/>
  ![Alt](duma_7.png "Title")



5. At any time you can launch **IBM Aspera Connect** to display the status of uploads to or downloads from your computer. <br/>
  ![Alt](duma_8.png "Title")



6. All data are encrypted on server side and they stay encrypted also upon download. For decryption, you have to navigate into your **IBM Aspera Connect** window and click "**Unlock encrypted files**". <br/>
  ![Alt](duma_9.png "Title") <br/>
You will be prompted for encryption passphrase which you should already have received from the repository administrator. <br/>
  ![Alt](duma_10.png "Title") <br/>
Encrypted files are by default kept on your disc after decryption. If you want to change this behaviour, navigate to Options->Settings and check "Delete encrypted files when finished" box.



<a name="DUMA_CLI"></a>
## Accessing DUMA via Command-Line Tool
In the following steps we describe instructions on how to use DUMA web interface.

1. **Install the aspera command line client.** The _ascp_ tool, which can be found [here](https://downloads.asperasoft.com/en/downloads/62) as instructed.

1. **Obtain tokens.** You will need **tokens** while using _ascp_. You will need a **download token** for download and a separate **upload token** for upload. Your tokens can be viewed by clicking the **Help** icon on your access link (see below).<br/>
     ![Alt](duma_help_link.png "Title") <br/><br/><br/>
     ![Alt](duma_cli_tokens.png "Title")

1. **Obtain encryption passphrase.** You should receive corresponding passphrase from repository administrator.

1. **Obtain SSH key.** You will also need an **SSH private key** to authenticate. This is the Aspera key that comes with the Aspera Connect installation named `asperaweb_id_dsa.openssh`. Below are locations where the ssh_key can be found:


    * On MAC
	  * Local installation of Aspera connect:
	    * `/Users/username/Applications/Aspera Connect.app/Contents/Resources/asperaweb_id_dsa.openssh`
	  * System wide installation of Connect:
	    * `/Applications/Aspera  Connect.app/Contents/Resources/asperaweb_id_dsa.openssh` 	

    * On WINDOWS
	  * Local installation of Aspera connect:
	    * `C:\\Users\username\AppData\Local\Programs\Aspera\Aspera Connect\etc\asperaweb_id_dsa.openssh` 	
	  * System wide installation of Connect:
	    * `C:\\Program Files (x86)\Aspera\Aspera Connect\etc\asperaweb_id_dsa.openssh`

    * On LINUX
	  * Local installation of Aspera connect:
	    * `/home/username/.aspera/connect/etc/asperaweb_id_dsa.openssh` 	
	  * System wide installation of Connect:
	    * `/opt/aspera/etc/asperaweb_id_dsa.openssh`

1. **Set necessary environment variables**.  `KEY` and `TOKEN` environment variables should be set as follows. If you do not have any of the tokens use `None` as the value.

	* `KEY=<path to the ssh key>`
	* `DTOKEN=<<<<YOUR DOWNLOAD TOKEN>>>>>>`
	* `UTOKEN=<<<<YOUR UPLOAD TOKEN>>>>>>`
	* `export ASPERA_SCP_FILEPASS=<<<<<YOUR ENCRYPTION PASSPHRASE>>>>>`

1. **Run the relevant _ascp_ commands**.


     * Upload:
		* To upload all *png* files to the root of the share:
		  * `ascp -d -i $KEY -P 33001 --file-crypt=encrypt -W $UTOKEN *.png app_duma_public_read@aspera.lcsb.uni.lu:/`
		* To upload *document.txt* to a subfolder:
		  * `ascp -d -i $KEY -P 33001 --file-crypt=encrypt -W $UTOKEN document.txt app_duma_public_read@aspera.lcsb.uni.lu:/subfolder`
	 * Download:
		* To download *subfolder* and all it's content to the current directory:
		  * `ascp -d -i $KEY -P 33001 --file-crypt=decrypt -W $DTOKEN app_duma_public_read@aspera.lcsb.uni.lu:subfolder`


You can find more information also on the [Aspera website](https://downloads.asperasoft.com/en/documentation/62).
